package com.uofc.feildapp;

import android.content.SharedPreferences;
import android.os.FileObserver;
import android.util.Log;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

import static com.uofc.feildapp.MainActivity.TAG;


public class myFileObserver extends FileObserver {
	public String absolutePath;
    private long timeInterval = 600; //The number of seconds between headers sent to RTEMP
	public myFileObserver(String path) {
		super(path, FileObserver.ALL_EVENTS);
		absolutePath = path;
	}
/**called to look for the event specified by the int, for more see fileobserver in the andorid docs.
 when called the method calls the last modified.
 TODO:
 -decide if the other events are still relevent.
*/
	public void onEvent(int event, String path) {

		//a new file or subdirectory was created under the monitored directory
		if ((FileObserver.CREATE == event)) {
//            Log.d(MainActivity.TAG, "File was created");
        }
		//a file or directory was opened
		if ((FileObserver.OPEN == event)) {
//            Log.d(MainActivity.TAG, "A file was openedl");
		}
		//data was read from a file
		if ((FileObserver.ACCESS == event)) {
//            Log.d(MainActivity.TAG, "File was accssed");
		}
		//data was written to a file
		if ((FileObserver.MODIFY == event)) {
//            Log.d(MainActivity.TAG, "File was modified");
		}
		//someone has a file or directory open read-only, and closed it
		if ((FileObserver.CLOSE_NOWRITE == event)) {
//            Log.d(MainActivity.TAG, "File Closed and not written to");
		}
		//someone has a file or directory open for writing, and closed it 
        if ((FileObserver.CLOSE_WRITE == event)) {
            /**
             * Here is where all the file watching takes place. When this event is triggered I go look in the path specified and look for the
             * last modified file. This is done using the method lastFileModified which is located at the bottom of this code. I take the last section of the
             * path to the last file for logging purposes and print it to the log cat. Then I get the unix time and look in the settings for the last save instance
             * of the last time sendToRTEMP was called. If it is greater then the number of seconds specified it calls the method and saves the current unix time
             * to the shared preference manager. Otherwise moveFile is called to send the incoming file to the appropriate directory on the hard drive.
             *
             * This has been threaded to ensure that if a second file is returned before this method finishes it can still be processed
             *
             */
            new Thread(new Runnable() {
                public void run() {

                    final File lastFile;
                    lastFile = lastFileModified(absolutePath);
                    if (lastFile != null){

                        Log.d(MainActivity.TAG, "last modified file is " + lastFile.toString());
                        String[] parts = lastFile.toString().split("/");
                        final String fileName = parts[parts.length-1];
                        Log.d(TAG, "last modified file name is " + fileName);
                        if (fileName == "test.txt" || fileName == "Test.txt") {
                            try {
                                MainActivity.testFileName(lastFile.toString(),fileName);
                            }catch (IOException e) {
                                Log.d(TAG,"IOE exception moving the test file");
                            }
                        }else {
                            long unixTime = System.currentTimeMillis() / 1000L;
                            long lastTime = MainActivity.settings.getLong("time",0L);
                            if ((unixTime - lastTime) > timeInterval) {
                                try {
                                    MainActivity.sendToRTEMP(lastFile.toString(),fileName);
                                }catch (ArrayIndexOutOfBoundsException a) {
                                    Log.d(TAG, "header not found!");
                                    a.printStackTrace();
                                }
                                SharedPreferences.Editor editor = MainActivity.settings.edit();
                                editor.putLong("time",unixTime);
                                editor.commit();
                            }else {
                                try{
                                    MainActivity.moveFile(lastFile.toString(),fileName);
                                    lastFile.delete();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    }
                }
            }).start();
        }
        //a file was deleted from the monitored directory
		if ((FileObserver.DELETE == event)) {
            //Log.d(MainActivity.TAG, "File was deleted");
		}
		//the monitored file or directory was deleted, monitoring effectively stops
		if ((FileObserver.DELETE_SELF == event)) {
//            Log.d(MainActivity.TAG, "monitered file or direactoy of delted and moitoring has stoped");
		}
//		//a file or subdirectory was moved from the monitored directory
		if ((FileObserver.MOVED_FROM == event)) {
//            Log.d(MainActivity.TAG, "file or subdirectory was moved");
		}
		//a file or subdirectory was moved to the monitored directory
		if ((FileObserver.MOVED_TO == event)) {
//            Log.d(MainActivity.TAG, "File or subdirectory was moved into the monitred directory");
		}
		//the monitored file or directory was moved; monitoring continues
		if ((FileObserver.MOVE_SELF == event)) {
//          Log.d(MainActivity.TAG, "File or directory was moved");
		}
		//Metadata (permissions, owner, timestamp) was changed explicitly
		if ((FileObserver.ATTRIB == event)) {
//            Log.d(MainActivity.TAG, "permisions, owner, or timestamp was changed explicity");
        }
}

    /**
     * This method looks for the last modified file in the directory by getting a list of all the files and then going one by one looking at the time stamps
     * if the current timestamp is newer than the previous it saves it other wise it just moves on to the next file in the list and returns the newest file.
     */
    public static File lastFileModified(String dir) {
        File fl = new File(dir);
        File[] files = fl.listFiles(new FileFilter() {
           public boolean accept(File file) {
               return file.isFile();
           }
        });
        long lastMod = Long.MIN_VALUE;
        File newFile = null;
        try {
        for (File file : files) {
            if (file.lastModified() > lastMod) {
                newFile = file;
                lastMod = file.lastModified();
            }
        }
        return newFile;
        }catch (NullPointerException n) {
            n.printStackTrace();
            return null;
        }
    }
}