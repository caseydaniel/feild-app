package com.uofc.feildapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

//This waits for the boot to be completed, then calls the main activity to start once the device is started
public class BootCompletedIntentReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

}