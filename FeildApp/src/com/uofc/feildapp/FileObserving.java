package com.uofc.feildapp;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.File;

/**
 * Created by Casey on 23/08/13.
 * This class puts the file observer and the battery listener in the background service. This way
 * I know it's always running even after extended periods of time.
 */
public class FileObserving extends Service {
    public SharedPreferences settings;
    public static int bhealth;
    public static int blevel;
    public static int bplugged;
    public static int bscale;
    public static int bstatus;
    public static int btemperature;
    public static int bvoltage;




    public int onStartCommand(Intent intent, int flags, int startId) {
        //This is the equivalent of onCreate. The battery info receiver is registered and the
        //method that starts the  file observer is called
        Log.i(MainActivity.TAG,"starting service");
        settings = PreferenceManager.getDefaultSharedPreferences(this);
        StartFileObserver();
        this.registerReceiver(this.batteryInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        //returning the START_STICKY ensures that when the process is killed onStartCommand is recalled
        //Note if this is ever modified it will be re-called with a null intent.
        return START_STICKY;
    }
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressWarnings("deprecation")
    public void StartFileObserver() {

        //creates the running notification and puts the processes in the foreground
        Notification notification = new Notification(R.drawable.ic_launcher, "Running the field app",
                System.currentTimeMillis());
        Intent notificationIntent = new Intent(this, FileObserving.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        notification.setLatestEventInfo(this, "Running the Field app",
                "Now running!", pendingIntent);
        startForeground(1, notification);
        Log.d(MainActivity.TAG,"starting File Observer");

        //Finds the directory in the settings
        String externalPath = settings.getString("raw data directory","/storage/usbdisk0/rawData/");
        File file2 = new File(externalPath);
        if (!file2.isDirectory()) {
            file2.mkdirs();
            Log.d(MainActivity.TAG, "making directory!");
        }
        String path = file2.getAbsolutePath();
        Log.d(MainActivity.TAG, path + " in main activity");
        //starts the file observer
        myFileObserver file = new myFileObserver(path);
        file.startWatching();

        //onEvent needs the integer corresponding to what you want the event to be. Path is to be the directory to be observed
        file.onEvent(myFileObserver.CLOSE_WRITE, path);
        new myFileObserver(path);
        Log.d(MainActivity.TAG, "starting the file observer at " + path);
    }

    protected BroadcastReceiver batteryInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // when an event is triggered this method is called, and new
            //values are stored in the shared preferences
            bhealth= intent.getIntExtra(BatteryManager.EXTRA_HEALTH,0);
            blevel= intent.getIntExtra(BatteryManager.EXTRA_LEVEL,0);
            bplugged= intent.getIntExtra(BatteryManager.EXTRA_PLUGGED,0);
            bscale= intent.getIntExtra(BatteryManager.EXTRA_SCALE,0);
            bstatus= intent.getIntExtra(BatteryManager.EXTRA_STATUS,0);
            btemperature= intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,0);
            bvoltage= intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE,0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("health",String.valueOf(bhealth));
            editor.putString("status",String.valueOf(bstatus));
            editor.putString("voltage",String.valueOf(bvoltage));
            editor.putString("temp",String.valueOf(btemperature));
            editor.commit();
        }
    };

}
