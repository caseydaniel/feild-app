package com.uofc.feildapp.java.transmit;

import com.uofc.feildapp.java.RTEMPPacketDescriptor;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class UDP extends Transmitter {

	/**
	 * Constructor that runs the super's constructor
	 */
	public UDP() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.rtemp.java.transmit.Transmitter#send(ca.rtemp.java.RTEMPPacketDescriptor
	 * )
	 */
	@Override
	public void send(RTEMPPacketDescriptor pd) {
		/*
		 * If the destination port is 25000, then we can assume **right now**
		 * that this is a "monitor" packet, and therefore, need to generate a
		 * special "monitor" data HashMap entry
		 */
		if (pd.getDestinationPort() == 25000) {
			// generate a "monitor" HashMap entry
			System.out.println("Generating \"monitor\" HashMap entry");
			String masterIDString = buildMasterIDString(pd);
			pd.addData("monitor", masterIDString);
			System.out.println(masterIDString);

			// write data to file
			System.out.println("\nWriting data to file '"
					+ pd.getPacketFilename() + "'");
			String stringifiedData = super.writePacketToString(pd);

			// transmit packet using file
			transmitStringData(super.norstarInetAddr, pd.getDestinationPort(),
					pd.getLocalhostPort(), stringifiedData);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.rtemp.java.transmit.Transmitter#transmitFileData(java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	protected void transmitFileData(InetAddress destinationAddress,
			int destinationPortNumber, int localhostPort, String filename) {
		try {
			// init reader and socket
			BufferedReader br = new BufferedReader(new FileReader(filename));
			DatagramSocket ds = new DatagramSocket();

			// prep data into byte array
			byte[] dataBytes = new byte[1490];
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			String data = sb.toString();
			dataBytes = data.getBytes();

			// send the data
			DatagramPacket sendPacket = new DatagramPacket(dataBytes,
					dataBytes.length, destinationAddress, destinationPortNumber);
			System.out.println("\nSending data by UDP to " + destinationAddress
					+ " at port number " + destinationPortNumber);
			ds.send(sendPacket);

			// close the socket and buffered reader
			ds.close();
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.rtemp.java.transmit.Transmitter#transmitStringData(java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	protected void transmitStringData(InetAddress destinationAddress,
			int destinationPortNumber, int localhostPort, String stringifiedData) {
		try {
			// init
			DatagramSocket ds = new DatagramSocket();

			// prep data into byte array
			byte[] dataBytes = new byte[1490];
			dataBytes = stringifiedData.getBytes();

			// send the data
			DatagramPacket sendPacket = new DatagramPacket(dataBytes,
					dataBytes.length, destinationAddress, destinationPortNumber);
			System.out.println("\nSending data by UDP to " + destinationAddress
					+ " at port number " + destinationPortNumber);
			ds.send(sendPacket);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Build the master ID string for a "monitor" packet
	 * 
	 * @param pd
	 *            Packet descriptor for this packet
	 * @return The built master ID string
	 */
	private String buildMasterIDString(RTEMPPacketDescriptor pd) {
		// init string builder
		StringBuilder sb = new StringBuilder();

		/*
		 * Build the master ID string using the already implemented standard of:
		 * monitor 1365876037 version 2.0 project themis site tpas device gbo-06
		 * date 2013-04-13 time 18:00:37 PACKET_NUMBER 29519 QUEUE_LENGTH 2
		 */
		sb.append("monitor " + pd.getUnixTime());
		sb.append(" version " + pd.getVersion());
		sb.append(" " + pd.getLevel1Alias() + " " + pd.getLevel1());
		sb.append(" " + pd.getLevel2Alias() + " " + pd.getLevel2());
		sb.append(" " + pd.getLevel3Alias() + " " + pd.getLevel3());
		sb.append(" date " + pd.getDate());
		sb.append(" time " + pd.getTime());
		sb.append(" PACKET_NUMBER " + pd.getPacketNumber());
		sb.append(" QUEUE_LENGTH " + pd.getQueueLength());

		// return value
		return sb.toString();
	}
}
