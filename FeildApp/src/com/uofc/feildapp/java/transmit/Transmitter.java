package com.uofc.feildapp.java.transmit;

import com.uofc.feildapp.java.RTEMPPacketDescriptor;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.InetAddress;
import java.net.UnknownHostException;

public abstract class Transmitter {

	public static final String externalIPAddressURL = "http://automation.whatismyip.com/n09230945.asp";

	/**
	 * Norstar-rt address information
	 */
	public InetAddress norstarInetAddr;

	/**
	 * Themis-rt address information
	 */
	public InetAddress themisInetAddr;

	/**
	 * CGSM-rt address information
	 */
	public InetAddress cgsmInetAddr;

	/**
	 * Constructor method
	 */
	public Transmitter() {
		// init Inet addresses for destination hosts
		try {
			this.norstarInetAddr = InetAddress
					.getByName("norstar-rt.phys.ucalgary.ca");
			this.themisInetAddr = InetAddress
					.getByName("themis-rt.phys.ucalgary.ca");
			this.cgsmInetAddr = InetAddress
					.getByName("cgsm-rt.phys.ucalgary.ca");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Send an {@link RTEMPPacketDescriptor} object to the RTEMP data centers
	 * 
	 * @param pd
	 */
	public abstract void send(RTEMPPacketDescriptor pd);

	/**
	 * Takes a text file on the filesystem and transmits the data using the
	 * protocol of the subclass
	 * 
	 * @param destinationAddress
	 *            IP address of the destination host
	 * @param destinationPortNumber
	 *            Port number of the destination host
	 * @param localhostPort
	 *            Outgoing port number to be used
	 * @param filename
	 *            Filename of the text file on the filesystem
	 */
	protected abstract void transmitFileData(InetAddress destinationAddress,
			int destinationPortNumber, int localhostPort, String filename);

	/**
	 * Takes a HashMap transmits the data using the protocol of the subclass
	 * 
	 * @param destinationAddress
	 *            IP address of the destination host
	 * @param destinationPortNumber
	 *            Port number of the destination host
	 * @param localhostPort
	 *            Outgoing port number to be used
	 * @param stringifiedData
	 *            Data to be transferred in string format
	 */
	protected abstract void transmitStringData(InetAddress destinationAddress,
			int destinationPortNumber, int localhostPort,
			String stringifiedData);

	/**
	 * Write the data in the packet descriptor to a file
	 * 
	 * @param pd
	 * @return Packet filename
	 */
	public String writePacketToFile(RTEMPPacketDescriptor pd) {
		try {
			// init
			Writer writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(pd.getPacketFilename())));

			// check to see if a "monitor" key exists
			if (pd.getData().containsKey("monitor")) {
				writer.write(pd.getData().get("monitor") + "\n");
			}

			// write all other data keys
			for (String key : pd.getData().keySet()) {
				if (!(key.equals("monitor"))) {
					writer.write(key + " " + pd.getData().get(key) + " ");
				}
			}

			// close up the writer
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// return filename
		return pd.getPacketFilename();
	}

	/**
	 * Write the packet data to a string (for sending it directly
	 * from a String object). This is to be used on Android devices
	 * primarily
	 * 
	 * @param pd
	 * @return
	 */
	public String writePacketToString(RTEMPPacketDescriptor pd) {
		// init
		StringBuilder sb = new StringBuilder();

		// append data to string builder
		// check to see if a "monitor" key exists
		if (pd.getData().containsKey("monitor")) {
			sb.append(pd.getData().get("monitor") + "\n");
		}

		// write all other data keys
		for (String key : pd.getData().keySet()) {
			if (!(key.equals("monitor"))) {
				sb.append(key + " " + pd.getData().get(key) + " ");
			}
		}

		// return
		return sb.toString();
	}
}
