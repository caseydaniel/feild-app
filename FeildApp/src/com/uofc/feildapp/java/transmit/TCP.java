package com.uofc.feildapp.java.transmit;

import com.uofc.feildapp.java.RTEMPPacketDescriptor;

import java.net.InetAddress;

public class TCP extends Transmitter {

	/**
	 * Constructor that runs the super's constructor
	 */
	public TCP() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.rtemp.java.transmit.Transmitter#send(ca.rtemp.java.RTEMPPacketDescriptor
	 * )
	 */
	@Override
	public void send(RTEMPPacketDescriptor pd) {
		// TODO Auto-generated method stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.rtemp.java.transmit.Transmitter#transmitFileData(java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	protected void transmitFileData(InetAddress destinationAddress,
			int destinationPortNumber, int localhostPort, String filename) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.rtemp.java.transmit.Transmitter#transmitStringData(java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	protected void transmitStringData(InetAddress destinationAddress,
			int destinationPortNumber, int localhostPort, String stringifiedData) {
		// TODO Auto-generated method stub

	}

}
