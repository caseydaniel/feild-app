package com.uofc.feildapp.java;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * This is the standard RTEMP object.
 * 
 */
public class RTEMPPacketDescriptor {

	// class variables
	public static Date now = new Date();
	public static SimpleDateFormat dateFormatter = new SimpleDateFormat();
	public static final String DATE_PATTERN = "yyyy-MM-dd";
	public static final String TIME_PATTERN = "HH:mm:ss";
	public static final String[] SUPPORTED_NETWORK_PROTOCOLS = { "tcp", "udp" };
	public static final String[] SUPPORTED_PACKET_FORMATS = { "json", "xml",
			"ascii_delimeter_based" };

	// primary key values for RTEMP server recognition
	private String level1 = "";
	private String level2 = "";
	private String level3 = "";
	private String level1Alias = "project";
	private String level2Alias = "site";
	private String level3Alias = "device";

	// values for networking information for this packet
	private String networkProtocol = "udp";
	private int localhostPort = 0;
	private int destinationPort = 0;

	// values for determining how to format the packet
	private String packetFormat = "json";
	private char delimeter = ' '; // SPACE character
	private boolean minified = true;

	// data
	private HashMap<String, String> data = new HashMap<String, String>();

	// output to filesystem details
	private String packetFilename = "";

	// date values
	private long unixTime = RTEMPPacketDescriptor.now.getTime() / 1000L;
	private String date = "";
	private String time = "";

	// other values
	private String version = "2.0";
	private int packetNumber = 0;
	private int queueLength = 0;

	/**
	 * @return the unixTime
	 */
	public long getUnixTime() {
		return unixTime;
	}

	/**
	 * @return the level1
	 */
	public String getLevel1() {
		return level1;
	}

	/**
	 * @return the level2
	 */
	public String getLevel2() {
		return level2;
	}

	/**
	 * @return the level3
	 */
	public String getLevel3() {
		return level3;
	}

	/**
	 * @return the level1Alias
	 */
	public String getLevel1Alias() {
		return level1Alias;
	}

	/**
	 * @return the level2Alias
	 */
	public String getLevel2Alias() {
		return level2Alias;
	}

	/**
	 * @return the level3Alias
	 */
	public String getLevel3Alias() {
		return level3Alias;
	}

	/**
	 * @return the networkProtocol
	 */
	public String getNetworkProtocol() {
		return networkProtocol;
	}

	/**
	 * @return the localhostPort
	 */
	public int getLocalhostPort() {
		return localhostPort;
	}

	/**
	 * @return the destinationPort
	 */
	public int getDestinationPort() {
		return destinationPort;
	}

	/**
	 * @return the packetFormat
	 */
	public String getPacketFormat() {
		return packetFormat;
	}

	/**
	 * @return the delimeter
	 */
	public char getDelimeter() {
		return delimeter;
	}

	/**
	 * @return the minified
	 */
	public boolean isMinified() {
		return minified;
	}

	/**
	 * @return the data
	 */
	public HashMap<String, String> getData() {
		return data;
	}

	/**
	 * @return the packetFilename
	 */
	public String getPacketFilename() {
		return packetFilename;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @return the packetNumber
	 */
	public int getPacketNumber() {
		return packetNumber;
	}

	/**
	 * @return the queueLength
	 */
	public int getQueueLength() {
		return queueLength;
	}

	/**
	 * @param unixTime the unixTime to set
	 */
	public void setUnixTime(long unixTime) {
		this.unixTime = unixTime;
	}
	
	/**
	 * @param level1
	 *            the level1 to set
	 */
	public void setLevel1(String level1) {
		this.level1 = level1;
	}

	/**
	 * @param level2
	 *            the level2 to set
	 */
	public void setLevel2(String level2) {
		this.level2 = level2;
	}

	/**
	 * @param level3
	 *            the level3 to set
	 */
	public void setLevel3(String level3) {
		this.level3 = level3;
	}

	/**
	 * @param level1Alias
	 *            the level1Alias to set
	 */
	public void setLevel1Alias(String level1Alias) {
		this.level1Alias = level1Alias;
	}

	/**
	 * @param level2Alias
	 *            the level2Alias to set
	 */
	public void setLevel2Alias(String level2Alias) {
		this.level2Alias = level2Alias;
	}

	/**
	 * @param level3Alias
	 *            the level3Alias to set
	 */
	public void setLevel3Alias(String level3Alias) {
		this.level3Alias = level3Alias;
	}

	/**
	 * @param networkProtocol
	 *            the networkProtocol to set
	 */
	public void setNetworkProtocol(String networkProtocol) {
		this.networkProtocol = networkProtocol;
	}

	/**
	 * @param localhostPort
	 *            the localhostPort to set
	 */
	public void setLocalhostPort(int localhostPort) {
		this.localhostPort = localhostPort;
	}

	/**
	 * @param destinationPort
	 *            the destinationPort to set
	 */
	public void setDestinationPort(int destinationPort) {
		this.destinationPort = destinationPort;
	}

	/**
	 * @param packetFormat
	 *            the packetFormat to set
	 */
	public void setPacketFormat(String packetFormat) {
		this.packetFormat = packetFormat;
	}

	/**
	 * @param delimeter
	 *            the delimeter to set
	 */
	public void setDelimeter(char delimeter) {
		this.delimeter = delimeter;
	}

	/**
	 * @param minified
	 *            the minified to set
	 */
	public void setMinified(boolean minified) {
		this.minified = minified;
	}

	/**
	 * @param packetFilename
	 *            the packetFilename to set
	 */
	public void setPacketFilename(String packetFilename) {
		this.packetFilename = packetFilename;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @param time
	 *            the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @param packetNumber
	 *            the packetNumber to set
	 */
	public void setPacketNumber(int packetNumber) {
		this.packetNumber = packetNumber;
	}

	/**
	 * @param queueLength
	 *            the queueLength to set
	 */
	public void setQueueLength(int queueLength) {
		this.queueLength = queueLength;
	}

	/**
	 * No argument constructor, default
	 */
	public RTEMPPacketDescriptor() {
		// set the date and time
		dateFormatter.applyPattern(DATE_PATTERN);
		setDate(dateFormatter.format(now));
		dateFormatter.applyPattern(TIME_PATTERN);
		setTime(dateFormatter.format(now));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// init string builder
		StringBuilder sb = new StringBuilder();

		// get each value and append to string builder
		sb.append("Level 1: " + getLevel1() + "\n");
		sb.append("Level 2: " + getLevel2() + "\n");
		sb.append("Level 3: " + getLevel3() + "\n");
		sb.append("Level 1 Alias: " + getLevel1Alias() + "\n");
		sb.append("Level 2 Alias: " + getLevel2Alias() + "\n");
		sb.append("Level 3 Alias: " + getLevel3Alias() + "\n");

		sb.append("Network Protocol: " + getNetworkProtocol() + "\n");
		sb.append("Localhost Port: " + getLocalhostPort() + "\n");
		sb.append("Destination Port: " + getDestinationPort() + "\n");

		sb.append("Packet Format: " + getPacketFormat() + "\n");
		sb.append("Delimeter: '" + getDelimeter() + "'\n");
		sb.append("Minified: " + isMinified() + "\n");

		sb.append("Unix Time: " + getUnixTime() + "\n");
		sb.append("Date: " + getDate() + "\n");
		sb.append("Time: " + getTime() + "\n");
		
		sb.append("Version: " + getVersion() + "\n");
		sb.append("Packet Number: " + getPacketNumber() + "\n");
		sb.append("Queue Length: " + getQueueLength() + "\n");
		sb.append("Packet Filename: " + getPacketFilename() + "\n");

		// return the full string
		return sb.toString();
	}

	/**
	 * Use for adding a key/value item to the data HashMap
	 * 
	 * @param key
	 * @param value 
	 */
	public void addData(String key, String value) {
		this.data.put(key, value);
	}

	/**
	 * Use for removing a key from the data HashMap
	 * @param key
	 */
	public void removeData(String key) {
		this.data.remove(key);
	}
}
