package com.uofc.feildapp;

/**
 * TODO:
 * look into putting the app into a background service -DONE-
 * Decide what if any unused methods can just be
 */

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.StatFs;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.uofc.feildapp.java.RTEMPPacketDescriptor;
import com.uofc.feildapp.java.transmit.UDP;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends Activity {
    public static final String TAG = "MainActivity";
    public static Context cntx = null;
    public static SharedPreferences settings;
    public NotificationManager mNotificationManager;
    private PowerManager.WakeLock wakeLock;


    /**
     * onCreate method is where everything gets started. This is the first thing called
     * when the app is opened. things like the static context (cntx), notification manager,
     * shared preferences (settings), and the wakelock get initialized. This is also
     * where the paths get displayed in the TextView on the UI.
     */
    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cntx = this.getBaseContext();
        mNotificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        settings = PreferenceManager.getDefaultSharedPreferences(this);
        String externalPath = settings.getString("raw data directory","/storage/usbdisk0/rawData/");
        String dataPath = settings.getString("out directories","/storage/usbdisk0/");
        TextView textView2 = (TextView)findViewById(R.id.textview2);
        textView2.setText("Raw data directory is " + externalPath);
        TextView textView1 = (TextView)findViewById(R.id.textview1);
        textView1.setText("Data tree is located at " + dataPath);

        //Start a wakelock to make sure the phone does not sleep.
        PowerManager powerManager = (PowerManager)getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK,"Feild App");
        wakeLock.acquire();

        //starts the File observer service.
        Log.d(TAG,"making intent for file observer");
        Intent intent = new Intent(this,FileObserving.class);
        startService(intent);

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }




    //These Save methods just save the new paths from the edit text on the UI and restart the main Activity
    public void Save(View view) {
        EditText editText = (EditText)findViewById(R.id.editText1);
        String directory = editText.getText().toString();
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("out directories", directory);
        editor.commit();
        Intent intent = new Intent(this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
    public void Save2(View view) {
        EditText editText = (EditText)findViewById(R.id.editText2);
        String directory = editText.getText().toString();
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("raw data directory", directory);
        editor.commit();
        Intent intent = new Intent(this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }



    /**This method is called by the myFileObserver and sends the newest file through FTP.
     * Right now the FTP is set to go to my personal BOX account but all you need to do is change the strings to the desired ftp
     * The inputs are the file path and the file name of the newest file.
     *
     * BEEN REMOVED
     */
    public static void sendFile(final String fileName, String absolutePath) {

        MyFTPClient ftpClient = null;
        ftpClient = new MyFTPClient();
        final MyFTPClient finalFtpClient = ftpClient;
        // Edit these for the FTP server in question
        final String host = "ftp.box.com";
        final String userId = "bobruels44@hotmail.com";
        final String password = "538Buffalo";
        final int port = 21;
        boolean conected = ftpClient.ftpConnect(host, userId, password, port);
        if (conected == true) {
            Log.d(MainActivity.TAG, "ftp is connected");
        }
        if (conected == false) {
            Log.d(MainActivity.TAG, "ftp is not connected");
        }

        if (conected == true) {
//        final Context cntx2 = this.getBaseContext();
            new Thread(new Runnable() {
                public void run(){
                    boolean status = finalFtpClient.ftpUpload(fileName, fileName, "/", cntx);
                    if (status == true)  {
                        Log.d(MainActivity.TAG, "Upload success");
                    }
                    else {
                        Log.d(MainActivity.TAG, "Upload failed");
                    }
                }
            }).start();
        }


    }


    /**Method to send to a file to RTEMP. It will take in the path string.
     *Takes in the file path, and will call methods to find the newest file, break it down and  puts it inot managable bits.
     * I will be making this to suite the data that I will be using so it may need modification to your own needs.
     * There is now also a boolean value that changes to true while it is running through the sending processes. The file
     * observer handles things so that if it is in the middle of sending then it puts it in a que to send.
     */
    public static void sendToRTEMP(String absolutePath, String filename) throws ArrayIndexOutOfBoundsException{
//        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
//        String siteName = "test Device";
//        String imei =  telephonyManager.getDeviceId();

        //initialize RTEMP object and modify any values
        //sending = true;
        final RTEMPPacketDescriptor pd = new RTEMPPacketDescriptor();


        //output packet descriptor values
        System.out.println(pd);

        //Collects the unix time, total memory, free memory, and the battery info to send out
        long unixTime = System.currentTimeMillis() / 1000L;
        String unixTimeString = String.valueOf(unixTime);
        long freeMemory = FreeMemory();
        long totalMemory = TotalMemory();
        String fileHeader = getHeader(absolutePath, filename);
        try {
            if (fileHeader != null) {
                String[] fileHeaderList = fileHeader.split(",");
                String status = settings.getString("status","0");
                String voltage = settings.getString("voltage", "0");
                String temp = settings.getString("temp","0");

                //Sets the pd information
                pd.setLevel1("Above");  //project
                pd.setLevel2(fileHeaderList[8]);   //site
                pd.setLevel3("KANANASKIS_PHONE");  //device
                pd.setDestinationPort(25000);
                pd.setPacketFormat("ascii_delimeter_based");
                pd.setDelimeter(' ');
                pd.setPacketNumber(pd.getPacketNumber() + 1);
                pd.setPacketFilename("25000.dat");

                //add the information to the pd packet
                pd.addData("basic", unixTimeString);
                pd.addData("Voltage1", fileHeaderList[9]);
                pd.addData("voltage2",fileHeaderList[10]);
                pd.addData("voltage3",fileHeaderList[11]);
                pd.addData("instrument_temp",fileHeaderList[12]);
                pd.addData("battery_voltage",voltage);
                pd.addData("battery_temp",temp);
                pd.addData("battery_status",status);
                pd.addData("free_memory",String.valueOf(freeMemory));
                pd.addData("total_memory",String.valueOf(totalMemory));
                pd.addData("gps_fix",fileHeaderList[3]);
                pd.addData("time",fileHeaderList[1]);
                pd.addData("software_version",fileHeaderList[4]);
                pd.addData("firmware_version",fileHeaderList[5]);
                pd.addData("site_id",fileHeaderList[6]);
                pd.addData("antenna",fileHeaderList[7]);
                pd.addData("instrument_id",fileHeaderList[8]);
                pd.addData("clock_speed",fileHeaderList[13]);
                pd.addData("offset",fileHeaderList[2]);

                Log.d(TAG,pd.toString());

                /**
                 * use RTEMP API to send RTEMP object built above. Because an internet connection can not be
                 *done on the main thread a second thread is made to handle the connection and fires of the
                 * UDP packet
                 */
                Thread thread = new Thread(new Runnable() {
                    @Override
                public void run() {
                        try{
                        UDP udp = new UDP();
                        udp.send(pd);
                        } catch (Exception e) {
                            e.printStackTrace();
                         }
                        }
                });
                thread.start();

                Log.d(TAG, "attempted to send, go take a look!");
                //attempts to move the file and then delete the source file.
                try {
                  moveFile(absolutePath, filename);
                 File file = new File(absolutePath);
                    file.delete();
              }catch (IOException e) {
                    Log.d(TAG, "unable to move data file");
                    e.printStackTrace();
                }
            }
        }catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            try {
                testFileName(absolutePath,filename);
            }catch (IOException t) {
                t.printStackTrace();
            }
        }
    }





    //useful to just make sure you don't have a null file when installed.
    // NO LONGER USED.
    public void createDummyFile() {
        String externalPath = settings.getString("raw data directory","/storage/usbdisk0/rawData/");
        try {
            FileOutputStream fos;
            String file_content = "Uploaded worked? maybe?????";

            fos = openFileOutput(externalPath, MODE_PRIVATE);
            fos.write(file_content.getBytes());
            fos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    //This checks the network state of the phone. This can be used to see if connection is available.
    //NOT CURRENTLY USED
    public static boolean isNetworkAvailable() {
        boolean outcome = false;

        if (cntx != null) {
            ConnectivityManager cm = (ConnectivityManager) cntx
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo[] networkInfos = cm.getAllNetworkInfo();

            for (NetworkInfo tempNetworkInfo : networkInfos) {
                if (tempNetworkInfo.isConnected()) {
                    outcome = true;
                    break;
                }
            }
        }

        return outcome;
    }

    /**
     * This code takes in srcString as the path to the source file name. Takes the last 20 characters as the name of the file
     * (this is constant due to the FTP transfer from the instrument). Then it takes the contents of the file bit by bit and
     * moves it to the source location that will be dictated by the date. The code checks to see if the directory exists and
     * makes the tree if needed.
     */

    public static void moveFile(String srcString, String filename) throws IOException{


        File src = new File(srcString);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        Date date = new Date();
        String today = dateFormat.format(date);
        String externalPath = settings.getString("out directories","/storage/usbdisk0/");
        String dstString = externalPath + today;
        Log.d(TAG,"Path for external drive is " + dstString);
        File dst = new File(dstString + "/" + filename);
        File path = new File(dstString);
        if (path.isDirectory()) {
            Log.i(TAG, "Today's directory exists!");
        } else {
            Log.d(TAG, "Making directories! " + path.toString());
            path.mkdirs();
        }
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();

    }

    /**
     * Method takes in the file specified by the path, extracts the first section up until
     * the character "}" is found. If the file can not be split into sub-strings then it is thrown
     * in the test folder by testFileName.
     */
    public static String getHeader(String path,String fileName) {
        File src = new File(path);
        Writer writer = new StringWriter();
        char buffer;
        try {
            InputStream in = new FileInputStream(src);
            while (in.available() > 0) {
                buffer = (char) in.read();
                writer.write(buffer);
                if(Character.toString(buffer) == "}") {
                    break;
                }
            }
            String string = writer.toString();
            return string.substring(1,string.length() - 1);
        }catch(IOException e) {
            e.printStackTrace();
            return null;
        }catch (StringIndexOutOfBoundsException s) {
            s.printStackTrace();
            Log.e(TAG, "HEADER COULD NOT BE FOUND!!!!!");
            try {
                testFileName(path,fileName);
            }catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    //These methods use android's StatFs to get the storage of the external hard drive.
    public static long FreeMemory() {
        String externalPath = settings.getString("out directories","/storage/usbdisk0/");
        StatFs statFs = new StatFs(externalPath);
        return  ((long)statFs.getAvailableBlocks()*(long)statFs.getBlockSize())/(1024*1024);
    }
    public static long TotalMemory() {
        String externalPath = settings.getString("out directories","/storage/usbdisk0/");
        StatFs statFs = new StatFs(externalPath);
        return ((long)statFs.getBlockCount()*(long)statFs.getBlockSize())/(1024*1024);
    }

    /**
     * This method is here to handle files that a header can not be found. A file is taken in
     * at the specified source path (srcString). A test file is made to see if the directory
     * exists for the day in the testFiles directory. If the directory is not found then one is made.
     * Then the file is moved into there with the appending of the time to the file name. The source
     * file is then delted.
     */
    public static void testFileName(String srcString, String fileName) throws IOException{
        File src = new File(srcString);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        DateFormat format = new SimpleDateFormat("HH-mm-ss");

        Date date = new Date();
        String today = dateFormat.format(date);
        String time = format.format(date);
        String externalPath = settings.getString("out directories","/storage/usbdisk0/");
        String dstString = externalPath + "testFiles/" + today;
        Log.d(TAG,"Path for external drive is " + dstString);
        File dst = new File(dstString + "/" + time + "-"+ fileName);
        File path = new File(dstString);
        if (path.isDirectory()) {
            Log.i(TAG, "Today's directory exists!");
        } else {
            Log.d(TAG, "Making directories! " + path.toString());
            path.mkdirs();
        }
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
        src.delete();


    }

}